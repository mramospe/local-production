#!/bin/python3
'''
Generate a Monte Carlo sample using a single machine.
'''
import argparse
import collections
import logging
import multiprocessing
import numpy as np
import os
import subprocess

SCRIPT = 'merge.py'

logger = logging.getLogger(__name__)


def merge(queue, jc):
    '''
    Function to merge a set of files allowing for parallelization.
    '''
    while True:

        with queue.lock:

            if queue.exit_signal.is_set():
                return

            i, fs = queue.get()

            if queue.empty():
                queue.exit_signal.set()

        directory = os.path.join('{jc.directory}',
                                 '{i:>0{queue.length}}').format(
                                     jc=jc, i=i, queue=queue)

        cmd = 'python {script} {files} --magnet {jc.magnet} --config {jc.configuration} --output {jc.output} '\
            '--directory {directory} --stdout stdout --stderr stderr'.format(script=SCRIPT, directory=directory, jc=jc, files=' '.join(fs))

        if subprocess.call(cmd.split()) != 0:
            logger.error(
                'Error during execution; see "{directory}/stderr" for more information'
                .format(directory=directory))


def main(files, nproc, output_directory, files_per_job, magnet, configuration,
         output):
    '''
    Main function to execute.
    '''
    files = [
        files[i:i + files_per_job] for i in range(0, len(files), files_per_job)
    ]

    # Fill the queue with the first event numbers for each job
    queue = multiprocessing.Queue()
    for i, l in enumerate(files):
        queue.put((i, l))

    # Save the number of items in the queue
    queue.length = len(str(len(files)))
    queue.lock = multiprocessing.Lock()
    queue.exit_signal = multiprocessing.Event()

    # Build a pool of processes and merge the files
    JobConfig = collections.namedtuple(
        'JobConfig', ['directory', 'magnet', 'configuration', 'output'])

    jc = JobConfig(output_directory, magnet, configuration, output)

    processes = [
        multiprocessing.Process(target=merge, args=(queue, jc), daemon=True)
        for _ in range(nproc)
    ]

    logger.info('Start processing')
    for p in processes:
        p.start()

    for p in processes:
        p.join()

    logger.info('Finished')


if __name__ == '__main__':

    logging.basicConfig(
        format='%(asctime)s - %(levelname)s: %(message)s', level=logging.INFO)

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('files', nargs='*', help='Files to merge')
    parser.add_argument(
        '--nproc',
        '-n',
        type=int,
        default=1,
        help='Number of parallel processes to execute')
    parser.add_argument(
        '--output-directory', '-o', help='Where to put the output files')
    parser.add_argument(
        '--output', type=str, required=True, help='Name of the output file')
    parser.add_argument(
        '--magnet',
        type=str,
        default='down',
        choices=('up', 'down'),
        help='Magnet polarity')
    parser.add_argument(
        '--configuration',
        '-c',
        type=str,
        help='Path to the configuration file')
    parser.add_argument(
        '--files-per-job',
        '-f',
        type=int,
        default=2,
        help='Number of files to process per job')
    args = parser.parse_args()

    main(**vars(args))
