'''
Function and classes defining executables (runners).
'''
import collections
import core
import functools
import logging
import os
import re
import subprocess

logger = logging.getLogger()

Step = collections.namedtuple('Step', ['options', 'directory', 'file_types'])

RUNTIME_DIR = os.path.abspath(os.path.dirname(__file__))


def run_executable(name,
                   executable,
                   dry_run,
                   main_directory,
                   tags,
                   function_for_options,
                   platform=None,
                   environ=None,
                   input_data=None,
                   remove_input_data=False,
                   subdir=None,
                   use_tmp=False,
                   env_vars=None,
                   **kwargs):
    '''
    Main function to run an executable with different option files.
    '''
    if env_vars is not None:
        env = {}
        for k in env_vars:
            if k == 'PATH':
                env[k] = '{os.environ["PATH"]}:/usr/bin'
            else:
                env[k] = os.environ[k]
    else:
        env = {'PATH': '/usr/bin'}

    # expose the current directory
    env['MCCONDOR_RUNTIME_DIR'] = RUNTIME_DIR

    set_environ = ['export HOME=~/', 'cd {directory}']

    if platform is not None:
        if 'slc5' in platform:
            # LbEnv is not supported for this old versions of the software, so
            # we must use the old LbLogin + SetupProject approach
            set_environ.append(
                f'source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh -c {platform}')
        else:
            set_environ += [
                'source /cvmfs/lhcb.cern.ch/lib/etc/cern_profile.sh',
                f'lb-set-platform {platform}',
            ]

    if environ:  # additional environment variables
        set_environ += environ

    if subdir is None:
        directory = os.path.join(main_directory, name)
    else:
        directory = os.path.join(main_directory, name, subdir)

    # Symbol to separate the commands (to create a not-so-ugly printout with --dry-run)
    if dry_run:
        sym = '\n'
    else:
        sym = ';'

    # Determine if the input data must be removed
    if not dry_run and input_data is not None and remove_input_data:
        rm_data = input_data
    else:
        rm_data = None

    with core.output_directory(directory, use_tmp, rm_data, dry_run) as cwd:

        # Add more options (tags, input data, ...)
        extra_options = [tags]

        # Define input data from previous step, if any
        if input_data is not None:

            data_options = os.path.join(cwd, 'data.py')
            with open(data_options, 'wt') as f:
                f.write("from GaudiConf import IOExtension\n"\
                        f"IOExtension().inputFiles(['{input_data}'], clear=True)\n")
                extra_options.append(data_options)

        # Additional arguments passed to the runner
        kwargs['main_directory'] = main_directory
        kwargs['runtime_directory'] = RUNTIME_DIR
        kwargs['cwd'] = cwd
        kwargs['dry_run'] = dry_run

        step = function_for_options(**kwargs)

        run_cmd = (sym.join(set_environ) +
                   sym +
                   ' '.join([executable, step.options] +
                            extra_options)).format(directory=cwd)

        # Run the application
        if dry_run:
            logger.info('Command to execute:\n' + run_cmd)
            data = os.path.join(step.directory, str(step.file_types))
        else:
            logger.info('Executing command:\n' + run_cmd)

            # Execute the command
            with open(os.path.join(cwd, 'stdout'), 'a') as stdout, \
                 open(os.path.join(cwd, 'stderr'), 'a') as stderr:

                process = subprocess.Popen(
                    run_cmd,
                    shell=True,
                    executable='/bin/bash',
                    stdout=stdout,
                    stderr=stderr,
                    env=env)
                error = (process.wait() != 0)

            if error:
                with open(os.path.join(cwd, 'stderr')) as stderr, open(
                        os.path.join(cwd, 'stdout')) as stdout:
                    raise RuntimeError(
                        f'Job finished with errors for "{name}":\n- stderr:\n{stderr.read() or "none"}- stdout:\n{stdout.read() or "none"}'
                    )
            else:
                logger.info('Job finalized successfully')

            # Return the data in the output directory, raise an error if more than one output file is found
            data = None
            for ft in step.file_types:
                for f in filter(
                        re.compile(ft).match,
                        os.listdir(os.path.join(step.directory))):
                    if data is not None:
                        raise RuntimeError(
                            f'More than one output file for "{name}"')
                    data = f

            if data is None:
                raise RuntimeError(
                    f'No output file has been found for "{name}" and expressions "{step.file_types}"'
                )

        # The name of the directory is that of the final destination
        return os.path.join(directory, data)


def run_lhcb(name, version, platform, packages=None, **kwargs):
    '''
    Run an application from an released official LHCb project. The configuration
    file for this mode must have the key "runner" filled as:

    {
    "type": "lhcb",
    "platform": "x86_64-slc6-gcc49-opt",
    "version": "v30r3",
    "packages": ["AppConfig.v3r379"]
    }

    Note that the key "type" can be dropped since "lhcb" is assumed as the
    default type by "runner". The "packages" argument is optional, and forces
    the use of the specified versions of the data packages.
    '''
    logger.info(f'Application "{name}/{version}"')

    if platform and 'slc5' in platform:
        if packages is not None:
            logger.error('Specifying extra packages is not supported for slc5 platforms; using defaults')
        executable = f'source SetupProject.sh {name} {version};gaudirun.py'
    else:
        packages = '' if packages is None else (' '.join([f'--use={p}' for p in packages]) + ' ')
        executable = f'lb-run {packages}{name}/{version} gaudirun.py'

    return run_executable(name, executable, platform=platform, **kwargs)


def run_local(name, executable, **kwargs):
    '''
    Run an application with a local executable. The configuration file for this
    mode must have the key "runner" filled as:

    {
    "type": "local",
    "executable": "/path/to/project/run",
    }
    '''
    executable += ' gaudirun.py'

    logger.info(f'Application "{name}" with executable "{executable}"')

    return run_executable(name, executable, **kwargs)


def runner(function_for_options):
    '''
    Decorate a function that returns a string of options.
    '''

    @functools.wraps(function_for_options)
    def _wrapper(runner, **kwargs):
        '''
        Internal wrapper to build and run an application. Arguments forwarded
        to the decorated function are those in "kwargs", together with "dry_run"
        and "cwd". The latter corresponds to the working directory for the runner."
        '''
        t = runner.pop('type', 'lhcb')

        kwargs.update(runner)

        if t == 'lhcb':
            run = run_lhcb
        elif t == 'local':
            run = run_local
        else:
            raise ValueError(f'Unknown runner type "{t}"')

        kwargs['function_for_options'] = function_for_options

        try:
            return run(**kwargs)
        except TypeError as ex:
            raise TypeError(
                f'Configuration for runner of type "{run.__name__}" not valid; check the documentation: {ex.message}'
            )

    return _wrapper


@runner
def gauss(event_type, first_event_number, run_number, nevts, year, polarity,
          cwd, options, dry_run, **kwargs):
    '''
    Return the string with the options to be passed to gaudirun.py.
    '''
    output_data = os.path.join(cwd, 'gauss.sim')

    logger.info(
        f'Gauss prepared with options "FirstEventNumber = {first_event_number}" and "RunNumber = {run_number}"'
    )

    extra_options = [
        'from Configurables import LHCbApp',
        f'LHCbApp().EvtMax = {nevts}',
        'from Gauss.Configuration import GenInit',
        'gauss = GenInit("GaussGen")',
        f'gauss.FirstEventNumber = {first_event_number}',
        f'gauss.RunNumber = {run_number}',
        'from Gauss.Configuration import OutputStream',
        f'OutputStream("GaussTape").Output = "DATAFILE=\'PFN:{output_data}\' TYP=\'POOL_ROOTTREE\' OPT=\'RECREATE\'"',
    ]

    path_to_extra_options = core.create_extra_options(cwd, extra_options,
                                                      dry_run)

    options.append(path_to_extra_options)

    options_str = core.parse_gaudirun_options(options).format(
        event_type=event_type, polarity=polarity, year=year, **kwargs)

    return Step(options_str, cwd, ['.*\.sim$'])


@runner
def boole(year, cwd, options, dry_run, **kwargs):
    '''
    Return the string with the options to be passed to gaudirun.py.
    '''
    name = os.path.join(cwd, 'boole')

    # The name of the output file in Boole is given from the dataset name
    # and the output type
    extra_options = [
        "from Configurables import Boole",
        f"Boole().DatasetName = '{name}'",
    ]

    path_to_extra_options = core.create_extra_options(cwd, extra_options,
                                                      dry_run)

    options.append(path_to_extra_options)

    options_str = core.parse_gaudirun_options(options).format(year=year, **kwargs)

    return Step(options_str, cwd, ['.*\.digi$', '.*\.xdigi$'])


@runner
def l0(year, tck, cwd, options, dry_run, **kwargs):
    '''
    Return the string with the options to be passed to gaudirun.py.
    '''
    output_data = os.path.join(cwd, 'l0.digi')

    extra_options = [
        "from Configurables import L0App",
        f"L0App().outputFile = '{output_data}'",
    ]

    path_to_extra_options = core.create_extra_options(cwd, extra_options,
                                                      dry_run)

    options.append(path_to_extra_options)

    options_str = core.parse_gaudirun_options(options).format(
        year=year, tck=tck, **kwargs)

    return Step(options_str, cwd, ['.*\.digi$', '.*\.xdigi$'])


@runner
def hlt(year, tck, cwd, options, dry_run, **kwargs):
    '''
    Return the string with the options to be passed to gaudirun.py.
    '''
    output_data = os.path.join(cwd, 'hlt.digi')

    extra_options = [
        "from Configurables import Moore",
        f"Moore().outputFile = '{output_data}'",
    ]

    path_to_extra_options = core.create_extra_options(cwd, extra_options,
                                                      dry_run)

    options.append(path_to_extra_options)

    options_str = core.parse_gaudirun_options(options).format(
        year=year, tck=tck, **kwargs)

    return Step(options_str, cwd, ['.*\.digi$', '.*\.xdigi$'])


@runner
def brunel(year, cwd, options, output_type, dry_run, **kwargs):
    '''
    Return the string with the options to be passed to gaudirun.py.
    '''
    name = os.path.join(cwd, 'brunel')

    # The name of the output file in Brunel is given from the dataset name
    # and the output type
    extra_options = [
        "from Configurables import Brunel",
        f"Brunel().DatasetName = '{name}'",
        f"Brunel().OutputType = '{output_type}'",
    ]

    path_to_extra_options = core.create_extra_options(cwd, extra_options,
                                                      dry_run)

    options.append(path_to_extra_options)

    options_str = core.parse_gaudirun_options(options).format(year=year, **kwargs)

    return Step(options_str, cwd, ['.*\.dst$', '.*\.ldst$'])


@runner
def davinci(year, cwd, options, dry_run, **kwargs):
    '''
    Return the string with the options to be passed to gaudirun.py.
    '''
    options_str = core.parse_gaudirun_options(options).format(year=year, **kwargs)

    return Step(options_str, cwd, ['.*\.dst$', '.*\.ldst$'])
