'''
Function and classes common for the different script.
'''
import contextlib
import functools
import json
import logging
import os
import shutil
import sys
import tempfile

from distutils.dir_util import copy_tree

logger = logging.getLogger()


def access_configuration(config, polarity):
    '''
    Access the configuration from the path to a JSON file, checking
    the tags.
    '''
    logger.info('Accessing configuration in: ' + config)
    with open(os.path.abspath(config)) as f:
        configuration = json.load(f)

    conddb = configuration['conddb']

    if polarity == 'md':
        if 'mu' in conddb:
            raise ValueError(
                'Specified a magnet-up CondDB tag whilst magnet polarity is set to "down"'
            )
    else:
        if 'md' in conddb:
            raise ValueError(
                'Specified a magnet-down CondDB tag whilst magnet polarity is set to "up"'
            )

    configuration['conddb'] = conddb.format(polarity=polarity)

    return configuration


def add_basic_arguments(parser):
    '''
    This function adds the following arguments to the parser:

    - directory: Where to locate the output files.
    - use_tmp: Whether to use the /tmp directory to create intermediate files.
    - magnet: Magnet polarity (down, up).
    - stdout: File to redirect the standard output of the script.
    - stderr: File to redirect the standard error output of the script.
    '''
    # General arguments
    parser.add_argument(
        '--directory',
        type=str,
        default='./',
        help='Place to put the output files')
    parser.add_argument('--use-tmp', action='store_true',
                        help='This option would make the script to use the /tmp '\
                        'directory to create the output files. Files will be '\
                        'copied to the directory specified with "--directory".')
    parser.add_argument('--dry-run', action='store_true',
                        help='This option will make the script print the '\
                        'configuration and exit without running any application')
    parser.add_argument(
        '--magnet',
        type=str,
        default='down',
        choices=('down', 'up'),
        help='Magnet polarity')

    # Logging arguments
    parser.add_argument('--stdout', type=str, default=None,
                        help='Path to a possible file to contain the standard '\
                        'output from the execution of the script')
    parser.add_argument('--stderr', type=str, default=None,
                        help='Path to a possible file to contain the standard '\
                        'error output from the execution of the script')


def create_extra_options(cwd, options, dry_run, filename='extra_options.py'):
    '''
    Write a set of extra options into a file.
    '''
    path_to_extra_options = os.path.join(cwd, filename)

    if not dry_run:
        with open(path_to_extra_options, 'wt') as f:
            for opt in options:
                f.write(opt + '\n')

    return path_to_extra_options


def make_directories(directory):
    '''
    In python 2.7 there is no "exists_ok" option in os.makedirs.
    '''
    if len(directory):
        try:
            os.makedirs(directory)
        except OSError as ex:
            if ex.errno != 17:
                # errno == 17 implies that the directory already exists
                raise ex


@contextlib.contextmanager
def output_directory(directory, use_tmp=False, rm_data=None, dry_run=False):
    '''
    Safe rule to create an output directory. Final files will be placed
    under "directory", whilst if "use_tmp" is set to True, files will
    be put in a temporary directory and copied on exit. Previous data
    can be removed if specified in "rm_data". It will be done only if
    the execution finalizes without errors.
    '''
    if use_tmp:

        tmpdir = tempfile.mkdtemp()

        try:
            yield tmpdir
        finally:
            if not dry_run:
                logger.info('Copying files to output directory')
                copy_tree(tmpdir, directory)
            shutil.rmtree(tmpdir)
    else:
        make_directories(directory)
        yield os.path.abspath(directory)

    if rm_data is not None:
        logger.info('Removing input data')
        os.remove(rm_data)


def parse_gaudirun_options(options):
    '''
    Parse a collection of options to be forwarded to gaudirun.py so it
    can run with environmental variables set after SetupProject or lb-run.
    '''
    return " ".join(map(lambda s: f"'{s}'", options))


def redirect_output(function):
    '''
    Redirect the python output created when executing the decorated function. This
    decorator is meant to be used by a "main" function, since it does not
    restore the attributes of the logger.
    '''

    @functools.wraps(function)
    def wrapper(stderr=None, stdout=None, **kwargs):
        '''
        Arguments in "kwargs" will be forwarded to the decorated function. If
        any of "stderr" or "stdout" is provided, the corresponding file will
        be created, that will contain the standard error/output of the execution
        of the function. This only corresponds to the output from python. Output
        from calls to C/C++ code, for example, will not be displayed.
        '''
        logger.setLevel(logging.INFO)

        formatter = logging.Formatter(
            '%(asctime)s - %(levelname)s: %(message)s')

        # Remove the current handlers
        old_handlers = logger.handlers
        logger.handlers = []

        if stdout is None:
            log_stdout = logging.StreamHandler(sys.stdout)
        else:
            make_directories(os.path.dirname(stdout))
            log_stdout = logging.FileHandler(stdout)

        log_stdout.setLevel(logging.INFO)

        if stderr is None:
            log_stderr = logging.StreamHandler(sys.stderr)
        else:
            make_directories(os.path.dirname(stderr))
            log_stderr = logging.FileHandler(stderr)

        log_stderr.setLevel(logging.WARNING)

        for s in (log_stdout, log_stderr):
            s.setFormatter(formatter)
            logger.addHandler(s)

        # Execute the function
        try:
            output = function(**kwargs)
        except Exception as ex:
            logger.exception(ex)
            raise ex
        else:
            logger.handlers = old_handlers
            return output
        finally:
            # Close the output files if created
            if stdout is not None:
                log_stdout.close()

            if stderr is not None:
                log_stderr.close()

    return wrapper


def sanitized_configuration(config, polarity):
    '''
    Check whether the input configuration has the applications correctly defined.
    '''
    configuration = access_configuration(config, polarity)

    # All characters will be lowercase
    apps = tuple(
        a['runner']['name'].lower() for a in configuration['applications'])

    if len(apps) == 0:
        raise ValueError(
            'No configuration for any application has been provided')

    if len(set(apps)) != len(configuration['applications']):
        logger.error('Duplicated applications in configuration file')

    # Check the configuration steps per year
    if configuration['year'].lower() == 'upgrade':
        order = ('gauss', 'boole', 'brunel', 'davinci')
    else:
        order = ('gauss', 'boole', 'moore', 'brunel', 'davinci')

    # To allow creating an n-tuple with MC-truth information
    if apps == ('gauss', 'davinci'):
        return configuration

    # Check the input applications
    for app in apps:
        if app not in order:
            raise RuntimeError(
                f'Unknown application "{app}"; choose among {order} for this data type'
            )

    # From the starting application, all the following must match
    first, last = apps[0], apps[-1]

    if first == last:
        return configuration

    order = order[order.index(first):order.index(last) + 1]

    for i, (prov, req) in enumerate(zip(apps, order)):
        if prov != req:
            raise RuntimeError(
                f'Application execution order requires to execute "{req}" after "{apps[i - 1]}" (provided: "{prov}")'
            )

    return configuration


def tags_options(dddb, conddb):
    '''
    Return the options needed to load the DDDB and CondDB tags.
    '''
    return [
        "from Configurables import LHCbApp",
        f"LHCbApp().DDDBtag = '{dddb}'",
        f"LHCbApp().CondDBtag = '{conddb}'",
    ]
