## Configurations to generate Monte Carlo samples

This directory holds different configuration options to generate and merge Monte Carlo samples.
Merge requests including additional files are more than welcome.
The directory is organized as follows:

- [generate](generate): Contains configuration files to generate Monte Carlo samples.
- [merge](merge): Contains configuration files to merge Monte Carlos samples.
- [test](test): Contains configuration files to test this package. Not meant to be utilized by users.
