#!/usr/bin/env python
'''
Script to format a set of JSON files in-place.
'''

import argparse
import json


def main( files ):
    '''
    Main function to execute.
    '''
    for f in files:
        with open(f, 'rt') as ifile:
            dct = json.load(ifile)
        with open(f, 'wt') as ofile:
            json.dump(dct, ofile, indent=4, sort_keys=True, separators=(',', ': '))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('files', nargs='+', help='JSON files to format')
    args = parser.parse_args()
    main(args.files)
