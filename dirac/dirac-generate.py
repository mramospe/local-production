#!/usr/bin/env python
'''
Script to submit production jobs to the GRID through DIRAC.
'''
import numpy
import os
import shutil
import tempfile

#################################
# Definition of the configuration
#################################

event_types = (
    30000000,
)

base_configuration = dict(
    # generation
    njobs=20,
    magnet='down',
    run_number=1,
    evts_per_job=1000,
    config='lhcb-Upgrade-Sim10b-LowMult-dirac.json',
    seed=17,
    # output files and location
    output_file='boole/*.digi',
    output_lfn_dir = '/lhcb/user/<user name path>/{event_type}/{process}'
    )

#####################################################################################


def main(event_type, njobs, evts_per_job, magnet, run_number, config, seed, output_file, output_lfn_dir):
    """
    Build a sequence of jobs to make simulated samples for a certain event type
    """
    job = Job()
    job.setName(f'Generate-{event_type}-mag{magnet}-{njobs}jobs-{evts_per_job}epj')
    job.setCPUTime(604800)
    job.setParameterSequence(
        'fen',
        (numpy.arange(njobs) * evts_per_job + seed).tolist())
    arguments = f'--event-type {event_type} --first-event-number %(fen)s --magnet {magnet} --run-number {run_number} --nevts {evts_per_job} --config {config} --remove-input-data --stdout stdout --stderr stderr --env-vars WORKDIR'
    job.setParameterSequence('jobid', list(map(f'{{:0{len(str(njobs))}}}'.format, range(njobs))))
    job.setInputSandbox(['decfiles', 'generate.py', 'core.py', 'runners.py', config])
    job.setOutputSandbox(['stdout', 'stderr'])
    job.setOutputData(output_file, outputPath=output_lfn_dir.format(event_type=event_type, process='%(jobid)s'))

    try:
        tmpdir = tempfile.mkdtemp()
        exe = os.path.abspath(os.path.join(tmpdir, 'executable.sh'))
        with open(exe, 'wt') as f:
            f.write(
'''#!/usr/bin/env bash
args=$@
set --
source /cvmfs/lhcb.cern.ch/lib/etc/cern_profile.sh
export WORKDIR=$PWD
python3 generate.py $args
''')
        job.setExecutable(exe, arguments=arguments)
        info = Dirac().submitJob(job)
    except e:
        raise e
    finally:
        shutil.rmtree(tmpdir)

    if 'Value' in info:
        print(f'{event_type} => ' + ' '.join(map(str, info['Value'])))


if __name__ == '__main__':

    from DIRAC.Core.Base import Script
    Script.parseCommandLine(ignoreErrors=False)
    from DIRAC.Interfaces.API.Job import Job
    from DIRAC.Interfaces.API.Dirac import Dirac

    for event_type in event_types:
        main(event_type=event_type, **base_configuration)
