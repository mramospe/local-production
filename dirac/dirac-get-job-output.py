#!/usr/bin/env python
'''
Retrieve the output sandbox of a job.
'''
import sys
import multiprocessing
from DIRAC.Core.Base import Script
Script.parseCommandLine(ignoreErrors=False)
from DIRAC.Interfaces.API.Dirac import Dirac
from DIRAC.Interfaces.API.Job import Job

dirac = Dirac()

failed_queue = multiprocessing.Queue()


def get_job_output(jobid):
    '''
    Get the output of the job with ID "jobid".
    '''
    sandbox = dirac.getOutputSandbox(jobid)
    output = dirac.getJobOutputData(jobid)

    if not sandbox['OK'] or not output['OK']:
        failed_queue.put((jobid, sandbox, output))


jobids = sys.argv[1:]

pool = multiprocessing.Pool(10)
pool.map(get_job_output, jobids)

while not failed_queue.empty():
    print('Job with ID {} failed:\n sandbox:{}\n output: {}'.format(
        *failed_queue.get()))
