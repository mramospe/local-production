#!/usr/bin/env python
'''
Monitor the given jobs in DIRAC
'''
import sys
from DIRAC.Core.Base import Script
Script.parseCommandLine(ignoreErrors=False)
from DIRAC.Interfaces.API.Dirac import Dirac
from DIRAC.Interfaces.API.Job import Job
dirac = Dirac()
for jobid in sys.argv[1:]:
    print('Status for job with ID {}: {}'.format(jobid,
                                                 dirac.getJobStatus(jobid)))
