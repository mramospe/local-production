#!/usr/bin/env python
'''
Script to submit jobs to merge files to the GRID through DIRAC.
'''

#################################
# Definition of the configuration
#################################

output_lfn_dir = ''

max_files_per_job = 2

configuration = dict(
    magnet='down',
    config='LDST-2016.json',
    output='merged.ldst',
)

# Build the job
import os
import shutil
import subprocess
import sys
import tempfile

if __name__ == '__main__':

    from DIRAC.Core.Base import Script
    Script.parseCommandLine(ignoreErrors=False)
    from DIRAC.Interfaces.API.Job import Job
    from DIRAC.Interfaces.API.Dirac import Dirac

    # Read the LFNs from the file
    with open(sys.argv[1]) as f:
        lfns_full = f.read().split('\n')
        if '' in lfns_full:
            lfns_full.remove('')

    dirac = Dirac()

    def chunks(lst, n):
        ''' Yield successive n-sized chunks from lst. '''
        for i in range(0, len(lst), n):
            yield lst[i:i + n]

    # Must create the catalog file
    for lfns in chunks(lfns_full, max_files_per_job):

        try:
            tmpdir = tempfile.mkdtemp()
            configuration['catalog'] = 'catalog.xml'
            catalog = os.path.join(tmpdir, configuration['catalog'])
            p = subprocess.Popen([
                'dirac-bookkeeping-genXMLCatalog', '-l', ' '.join(lfns),
                '--Catalog={}'.format(catalog)
            ],
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            if p.wait() != 0:
                _, stderr = p.communicate()
                raise RuntimeError(
                    'Problems generating the XML catalog:\n stderr: {}'.format(
                        stderr))

            # Make and submit the job
            job = Job()
            job.setName('Merge-{magnet}'.format(**configuration))
            job.setCPUTime(172800)
            arguments = '{lfns} --catalog {catalog} --output {output} --magnet {magnet} --config {config} --stdout stdout --stderr stderr'.format(
                lfns=' '.join(lfns), **configuration)
            job.setInputSandbox([
                'merge.py', 'core.py', 'runners.py',
                os.path.abspath(
                    os.path.join('configurations/merge', configuration['config'])),
                catalog
            ])
            job.setOutputSandbox(
                ['stdout', 'stderr', 'davinci_stdout', 'davinci_stderr'])
            job.setOutputData('*', outputPath=output_lfn_dir)

            exe = os.path.abspath(os.path.join(tmpdir, 'executable.sh'))
            with open(exe, 'wt') as f:
                f.write(
'''#!/usr/bin/env bash
args=$@
set --
source /cvmfs/lhcb.cern.ch/lib/etc/cern_profile.sh
python3 merge.py $args
''')
                job.setExecutable(exe, arguments=arguments)
                info = Dirac().submitJob(job)
        except e:
            raise e
        finally:
            shutil.rmtree(tmpdir)
