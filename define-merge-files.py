#!/usr/bin/env python
'''
Script to divide a set of input files in chunks and save them in an
output file. It is meant to be used together with CONDOR submit
configuration files.
'''
import argparse


def main(files, output, files_per_chunk, mode):
    '''
    Function to execute.
    '''
    with open(output, mode) as f:
        for i in range(0, len(files), files_per_chunk):
            f.write(' '.join(files[i:i + files_per_chunk]) + '\n')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('files', nargs='+', help='Input files')
    parser.add_argument(
        '--output',
        '-o',
        default='files.txt',
        help='Name of the output file with the files divided in chunks')
    parser.add_argument(
        '--files-per-chunk',
        '-n',
        type=int,
        default=100,
        help='Number of files to put per chunk')
    parser.add_argument(
        '--mode',
        '-m',
        default='a',
        choices=('a', 'w'),
        help='Mode for the output file to use')

    args = parser.parse_args()

    main(**vars(args))
