#!/bin/python3
'''
Generate a Monte Carlo sample.
'''
import argparse
import core
import runners
import logging
import os

logger = logging.getLogger()


@core.redirect_output
def main(event_type, nevts, first_event_number, run_number, magnet, config,
         directory, dry_run, output, remove_input_data, use_tmp, env_vars):
    '''
    Function to execute.
    '''
    if env_vars is not None:
        logger.info('Environment variables exposed:\n{}'.format('\n'.join(
            '- {} = {}'.format(k, os.environ[k]) for k in env_vars)))

    configuration = core.sanitized_configuration(config, magnet)

    # Must run Gauss, at least
    if 'gauss' not in [
            a['runner']['name'] for a in configuration['applications']
    ]:
        raise RuntimeError('At least must run Gauss to generate a MC sample')

    # Parse the tags
    dddb = configuration['dddb']
    conddb = configuration['conddb']

    directory = os.path.abspath(directory)

    # Create global options
    global_options_path = os.path.join(directory, 'global')

    tags_file = os.path.join(global_options_path, 'tags.py')

    if dry_run:
        logger.info(
            'Running in "dry-run" mode; will only print the commands to execute'
        )
    else:
        logger.info('Creating output directory')

        core.make_directories(global_options_path)

        with open(tags_file, 'wt') as f:
            for line in core.tags_options(dddb, conddb):
                f.write(line + '\n')

    applications = {
        a['runner']['name']: a
        for a in configuration['applications']
    }

    # Shortcut to create the essential configuration dictionary for an application
    def base_configuration(name):
        '''
        Return the minimum dictionary needed to declare a runner.
        '''
        cfg = dict(applications[name])
        cfg['year'] = configuration['year']
        cfg['main_directory'] = directory
        cfg['dry_run'] = dry_run
        cfg['remove_input_data'] = remove_input_data
        cfg['tags'] = tags_file
        cfg['use_tmp'] = use_tmp
        cfg['env_vars'] = env_vars
        return cfg

    # Configure and run Gauss
    gauss = base_configuration('gauss')
    gauss['event_type'] = event_type
    gauss['polarity'] = magnet
    gauss['nevts'] = nevts
    gauss['first_event_number'] = first_event_number
    gauss['run_number'] = run_number

    data = runners.gauss(**gauss)

    # Configure and run Boole
    if 'boole' in applications:
        boole = base_configuration('boole')
        data = runners.boole(input_data=data, **boole)

    # Configure and run Moore (2 or 3 steps)
    if 'moore' in applications:
        moore = base_configuration('moore')

        raw_moore = dict(moore)

        steps = raw_moore.pop('steps')

        # L0
        l0conf = dict(raw_moore)
        l0conf.update(steps['l0'])
        data = runners.l0(input_data=data, subdir='l0', **l0conf)

        if 'hlt' in steps:  # Run 1 / 2015 - like trigger
            # HLT
            hltconf = dict(raw_moore)
            hltconf.update(steps['hlt'])
            data = runners.hlt(input_data=data, subdir='hlt', **hltconf)
        else:
            # HLT1
            hlt1conf = dict(raw_moore)
            hlt1conf.update(steps['hlt1'])
            data = runners.hlt(input_data=data, subdir='hlt1', **hlt1conf)
            # HLT2
            hlt2conf = dict(raw_moore)
            hlt2conf.update(steps['hlt2'])
            data = runners.hlt(input_data=data, subdir='hlt2', **hlt2conf)

    # Configure and run Brunel
    if 'brunel' in applications:
        brunel = base_configuration('brunel')
        data = runners.brunel(input_data=data, **brunel)

    # Create the symbolic link
    if not dry_run and output is not None:
        output_path = os.path.join(directory, output)
        try:
            os.symlink(data, output_path)
        except OSError as ex:
            if ex.errno != 17:
                # errno == 17 implies that the link already exists
                raise ex
        else:
            logger.info('Created symbolic link to: {} -> {}'.format(
                output_path, data))

        logger.info('The production has been completed')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)

    core.add_basic_arguments(parser)

    # General options
    parser.add_argument('--output', type=str, default=None,
                        help='Output file name. A symbolic link wil be '\
                        'created with this name to the output from the last step.')
    parser.add_argument('--remove-input-data', action='store_true',
                        help='If set to true, once a step finalizes the data '\
                        'from the previous is removed.')
    parser.add_argument(
        '--config',
        type=str,
        required=True,
        default='configurations/lhcb-2016-Sim09l-0x6139160F-Reco16.json',
        help='Path to the configuration')
    parser.add_argument(
        '--env-vars',
        type=str,
        nargs='*',
        default=None,
        help='Environment variables to preserve')

    # Generation options
    parser.add_argument(
        '--event-type',
        type=int,
        default=30000000,
        help='Event type to process')
    parser.add_argument('--first-event-number', type=int, default=1,
                        help='Value to create the seed to be used by Gauss. '\
                        'The actual seed will be this number times the total '\
                        'number of events to process.')
    parser.add_argument('--run-number', type=int, default=1,
                        help='Run number associated to the Monte Carlo '\
                        'production. This is one of the seeds used by '\
                        'Gauss to generate statistically independent '\
                        'samples.')
    parser.add_argument(
        '--nevts', type=int, default=10, help='Number of events to generate')

    args = parser.parse_args()

    opts = vars(args)

    opts['magnet'] = 'md' if opts['magnet'] == 'down' else 'mu'

    main(**opts)
