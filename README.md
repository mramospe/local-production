# Generation of local Monte Carlo samples with the LHCb framework

[![pipeline status](https://gitlab.cern.ch/mramospe/mc-condor/badges/master/pipeline.svg)](https://gitlab.cern.ch/mramospe/mc-condor/-/pipelines)

This project contains configuration files and scripts to create Monte Carlo samples using the LHCb framework.
Configurations are saved under [configurations](configurations) in JSON format. The script [generate.py](generate.py) creates the simulated sample by reading these configurations.
The files [dirac/dirac-generate.py](dirac/dirac-generate.py), [dirac/dirac-merge.py](dirac/dirac-merge.py), [dirac/dirac-monitor-job.py](dirac/dirac-monitor-job.py) and [dirac/dirac-get-job-output.py](dirac/dirac-get-job-output.py) allow to submit and manage jobs sent to GRID through DIRAC.
The file [condor/generate.condor](condor/generate.condor) corresponds to a configuration file to submit jobs to CONDOR.

## Executing scripts locally

To have an idea of how the [generate.py](generate.py) script works, type:
```bash
python generate.py -h
```

The only required argument corresponds to the configuration to use.
```bash
python generate.py --config configurations/lhcb-2016-Sim09l-0x6139160F-Reco16.json
```
By default, the script generates minimum-bias events (event type 30000000), but this can be changed using the "--event-type" argument:
```bash
python generate.py --event-type 10000000 --config configurations/lhcb-2016-Sim09l-0x6139160F-Reco16.json
```
This way we would be generating a sample of inclusive b events.

Usually, it is common to parallelize Monte Carlo generation by creating many jobs, processing only a few events per job. This leads to a situation where one has too many files, becoming unmanageable.
This package provides a script to merge them, as long as they are in DST/LDST format, called [merge.py](merge.py). The behaviour is very similar to [generate.py](generate.py).
The mandatory arguments are the files to merge, the configuration file and the name of the output file.
A call like:
```bash
python merge.py f1.ldst f2.ldst --output merged.ldst --config configurations/merge/LDST-2016.json
```
will merge the files "f1.ldst" and "f2.ldst" into "merged.ldst" using the configuration in [configurations/merge/LDST-2016.json](configurations/merge/LDST-2016.json).
Any kind of DST-like files can be merged changing the configuration according to the new input type.

## Submitting jobs to GRID

This package provides four scripts to submit and manage jobs in the GRID submitted with DIRAC.

* [dirac/dirac-generate.py](dirac/dirac-generate.py): Submits jobs with the [generate.py](generate.py) script. The jobs IDs will be printed in the screen at the end of submission. This script must be modified according to the necessities of the user.
* [dirac/dirac-merge.py](dirac/dirac-merge.py): Submits jobs with the [merge.py](merge.py) script. The jobs IDs will be printed in the screen at the end of submission. This script must be modified according to the necessities of the user. As arguments it takes a TXT file with chunks of files to merge (it will create one job per chunk). See [define-merge-files.py](define-merge-files.py) for more details.
* [dirac/dirac-monitor.py](dirac/dirac-monitor.py): Allows to get the status of the jobs in the GRID. The job IDs must be provided as arguments to the script.
* [dirac/dirac-get-job-output.py](dirac/dirac-get-job-output.py): Get the output files and data from associated to a set of job IDs.

In order to run this scripts, one must have a valid GRID proxy enabled.
Within LHCb, scripts must be executed like
```bash
lb-dirac python <script> <options>
```

## Submitting jobs to CONDOR

The usage of CONDOR in LHCb is not recommended, since it interferes with the GRID management.
However, in cases of extreme necessity this can still be done.
In order to submit a bunch of jobs to CONDOR, one must modify the [condor/generate.condor](condor/generate.condor) script according to the necessities. Then one can simply type
```bash
condor_submit condor/generate.condor
```
The process of merging samples can be also done with:
```bash
condor_submit condor/merge.condor
```
The file [condor/merge.condor](condor/merge.condor) must be changed according to the input files being processed.
More information about CONDOR submission scripts can be found in the [documentation](https://htcondor.readthedocs.io/en/latest).

## Defining a TXT file with samples to merge

In order to specify the files to merge both with [dirac/dirac-merge.py](dirac/dirac-merge.py) and [condor/merge.condor](condor/merge.condor), one has to create a TXT file containing chunks of files to process in each call to [merge.py](merge.py).
The script [define-merge-files.py](define-merge-files.py) has been created for such purpose, where it is possible to process a set of files and divide them in equally-sized chunks.
In order to know more about its functionality type
```bash
python define-merge-files.py -h
```

## Using external configuration files

Sometimes you will want to upload configuration files that have not
been released yet. For that purpose, that path to your files must
be preceeded by `$MCCONDOR_RUNTIME_DIR`, which is an environment
variable that is set to the location of the script,
([generate.py](generate.py) or [merge.py](merge.py)) to be
executed.
