#!/bin/python3
'''
Generate a Monte Carlo sample using a single machine.
'''
import argparse
import collections
import logging
import multiprocessing
import numpy as np
import os
import subprocess

SCRIPT = 'generate.py'

logger = logging.getLogger(__name__)


def generate(queue, jc):
    '''
    Function to generate a single MC sample allowing parallelization.
    '''
    while True:

        with queue.lock:

            if queue.exit_signal.is_set():
                return

            i, fe = queue.get()

            if queue.empty():
                queue.exit_signal.set()

        directory = os.path.join('{jc.directory}',
                                 '{i:>0{queue.length}}').format(
                                     jc=jc, i=i, queue=queue)

        cmd = 'python {script} --event-type {jc.evt_type} --first-event-number {fe} '\
            '--magnet {jc.magnet} --run-number {jc.run_number} --nevts {jc.nevts} '\
            '--config {jc.configuration} --remove-input-data '\
            '--directory {directory} --stdout stdout --stderr stderr'.format(script=SCRIPT, directory=directory, jc=jc, fe=fe)

        if subprocess.call(cmd.split()) != 0:
            logger.error(
                'Error during execution; see "{directory}/stderr" for more information'
                .format(directory=directory))


def main(nproc, output_directory, njobs, evts_per_job, evt_type, magnet,
         configuration, first_evt_number, run_number):
    '''
    Main function to execute.
    '''
    # Fill the queue with the first event numbers for each job
    queue = multiprocessing.Queue()
    fens = np.arange(
        first_evt_number,
        njobs * evts_per_job + first_evt_number,
        step=evts_per_job)
    logger.info('Define the first event number for each job')
    for i, v in enumerate(fens):
        queue.put((i, v))

    logger.info('Generating using seeds in [{}, {})'.format(
        fens[0], fens[-1] + evts_per_job))

    # Save the number of items in the queue
    queue.length = len(str(len(fens)))
    queue.lock = multiprocessing.Lock()
    queue.exit_signal = multiprocessing.Event()

    # Build a pool of processes and generate the events
    JobConfig = collections.namedtuple('JobConfig', [
        'directory', 'nevts', 'evt_type', 'magnet', 'configuration',
        'run_number'
    ])

    jc = JobConfig(output_directory, evts_per_job, evt_type, magnet,
                   configuration, run_number)

    processes = [
        multiprocessing.Process(target=generate, args=(queue, jc))
        for _ in range(nproc)
    ]

    logger.info('Start processing')
    for p in processes:
        p.start()

    for p in processes:
        p.join()

    logger.info('Finished')


if __name__ == '__main__':

    logging.basicConfig(
        format='%(asctime)s - %(levelname)s: %(message)s', level=logging.INFO)

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        '--nproc',
        '-n',
        type=int,
        default=1,
        help='Number of parallel processes to execute')
    parser.add_argument(
        '--output-directory', '-o', help='Where to put the output files')
    parser.add_argument(
        '--njobs', '-j', default=1, type=int, help='Number of jobs to run')
    parser.add_argument(
        '--evts-per-job',
        '-p',
        type=int,
        default=1000,
        help='Number of events to generate per job')
    parser.add_argument(
        '--evt-type',
        '-e',
        type=int,
        default=30000000,
        help='Event type to generate')
    parser.add_argument(
        '--magnet',
        type=str,
        default='down',
        choices=('up', 'down'),
        help='Magnet polarity')
    parser.add_argument(
        '--configuration',
        '-c',
        type=str,
        help='Path to the configuration file')
    parser.add_argument(
        '--first-evt-number',
        '-f',
        type=int,
        default=1,
        help='First event number')
    parser.add_argument(
        '--run-number', '-r', type=int, default=1, help='Run number')
    args = parser.parse_args()

    main(**vars(args))
