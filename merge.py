#!/bin/python3
'''
Merge a set of input files into a single file.
'''
import argparse
import core
import logging
import os
import subprocess

logger = logging.getLogger()


@core.redirect_output
def main(files, directory, output, magnet, use_tmp, config, dry_run, catalog):
    '''
    Function to execute.
    '''
    configuration = core.access_configuration(config, magnet)

    if dry_run:
        logger.info(
            'Running in "dry-run" mode; will only print the commands to execute'
        )
    else:
        logger.info('Creating output directory')
        core.make_directories(directory)

    # Symbol to separate the commands (to create a not-so-ugly printout with --dry-run)
    if dry_run:
        sym = '\n'
    else:
        sym = ';'

    with core.output_directory(directory, use_tmp, dry_run=dry_run) as cwd:

        # Define the environment
        environ = [
            'cd {directory}',
            'export HOME=~/',
            'source /cvmfs/lhcb.cern.ch/lib/LbEnv.sh',
            'lb-set-platform {platform}',
        ]

        environ_cmds = sym.join(environ).format(
            directory=cwd, platform=configuration['platform']) + sym

        # Build the input options
        tags = os.path.join(cwd, 'tags.py')

        options = [
            '$APPCONFIGOPTS/Merging/DVMergeDST.py',  # The input type is modified later
            '$APPCONFIGOPTS/DaVinci/DataType-{year}.py',
            '$APPCONFIGOPTS/Merging/WriteFSR.py',
            '$APPCONFIGOPTS/Merging/MergeFSR.py',
            '$APPCONFIGOPTS/DaVinci/Simulation.py',
        ]

        output = os.path.join(cwd, output)

        extra_options = [
            "from Configurables import DaVinci",
            f"DaVinci().InputType = '{configuration['type']}'",
            "from Gaudi.Configuration import InputCopyStream",
            f"InputCopyStream().Output = \"DATAFILE='PFN:{output}' TYP='POOL_ROOTTREE' OPT='REC'\"",
            "from Configurables import RecordStream",
            f"RecordStream('FileRecords').Output = \"DATAFILE='PFN:{output}' TYP='POOL_ROOTTREE' OPT='REC'\"",
        ]

        if catalog is not None:
            extra_options += [
                "from Gaudi.Configuration import FileCatalog",
                f"FileCatalog().Catalogs = ['xmlcatalog_file:{catalog}']",
            ]

        path_to_extra_options = core.create_extra_options(
            cwd, extra_options, dry_run)

        options_str = core.parse_gaudirun_options(options).format(
            year=configuration['year'])

        # Define the input data
        data_options = os.path.join(cwd, 'data.py')

        cmd = f'lb-run DaVinci/{configuration["version"]} gaudirun.py'

        run_cmd = ' '.join([
            environ_cmds, cmd, options_str, path_to_extra_options, tags,
            data_options
        ])

        if dry_run:
            # Only print command to execute
            logger.info('Command to execute:\n' + run_cmd)
        else:
            # Run the application
            logger.info('Executing command:\n' + run_cmd)

            with open(tags, 'wt') as f:
                for line in core.tags_options(configuration['dddb'],
                                              configuration['conddb']):
                    f.write(line + '\n')

            with open(data_options, 'wt') as f:
                f.write("from GaudiConf import IOExtension\n"\
                        f"IOExtension().inputFiles({files}, clear=True)\n")

            with open(os.path.join(cwd, 'davinci_stdout'), 'a') as stdout, \
                 open(os.path.join(cwd, 'davinci_stderr'), 'a') as stderr:

                process = subprocess.Popen(
                    run_cmd,
                    shell=True,
                    executable='/bin/bash',
                    stdout=stdout,
                    stderr=stderr,
                    env={'PATH': '/usr/bin'})
                error = (process.wait() != 0)

            if error:
                with open(os.path.join(cwd, 'davinci_stderr')) as stderr, open(
                        os.path.join(cwd, 'davinci_stdout')) as stdout:
                    raise RuntimeError(
                        f'Job finished with errors:\n- stderr:\n{stderr.read() or "none"}\n- stdout:\n{stdout.read() or "none"}'
                    )
            else:
                logger.info('Job finalized successfully')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=__doc__)

    core.add_basic_arguments(parser)

    parser.add_argument('files', nargs='+', help='Files to merge')
    parser.add_argument(
        '--output', type=str, required=True, help='Name of the output file')
    parser.add_argument(
        '--config',
        type=str,
        required=True,
        default='merging/merge-2016.json',
        help='Path to the configuration')
    parser.add_argument('--catalog', type=str, required=False,
                        default=None,
                        help='Possible XML catalog to process. If provided, '\
                        'input files are considered to be LFNs.')

    args = parser.parse_args()

    opts = vars(args)

    opts['magnet'] = 'md' if opts['magnet'] == 'down' else 'mu'

    main(**opts)
